import React from "react";

function Copyright() {
  return (
    <div className="footer-copyright py-3">
      © 2019 Copyright:
      <span> radakuceru.eu</span>
    </div>
  );
}

export default Copyright;
