import React from "react";

function SocialMediaLink(props) {
  return (
    <span>
      <i className={props.class} />
    </span>
  );
}

export default SocialMediaLink;
