import React from "react";
import SocialMediaLink from "./SocialMediaLink";

function SocialMedias() {
  return (
    <div className="pb-4">
      <SocialMediaLink link="" class="fa fa-facebook mr-3" />
      <SocialMediaLink link="" class="fa fa-twitter mr-3" />
      <SocialMediaLink link="" class="fa fa-google-plus mr-3" />
      <SocialMediaLink link="" class="fa fa-instagram mr-3" />
      <SocialMediaLink link="" class="fa fa-steam mr-3" />
    </div>
  );
}

export default SocialMedias;
