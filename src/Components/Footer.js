import React from "react";
import Copyright from "./Footer/Copyright";
import SocialMedias from "./Footer/SocialMedias";

function Footer() {
  return (
    <footer className="page-footer text-center font-small mdb-color darken-2 mt-4 wow fadeIn">
      <hr className="my-4" />
      <SocialMedias />
      <Copyright />
    </footer>
  );
}

export default Footer;
