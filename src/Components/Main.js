import React from "react";
import MainSection from "./Main/MainSection";

function Main() {
  return (
    <main className="mt-5 pt-5">
      <div className="container">
        <MainSection />
      </div>
    </main>
  );
}

export default Main;
