import React from "react";
import Navbar from "./Header/Navbar";

function Header() {
  return (
    <header>
      <Navbar />
    </header>
  );
}

export default Header;
