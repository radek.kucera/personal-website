import React from "react";
import {
  MainText,
  AboutText,
  ContactText,
  GraphText
} from "../../Constants/Text";
import Card from "./Card";
import AboutCard from "./AboutCard";
import CustomCard from "./CustomCard";

function MainSection() {
  return (
    <section className="mt-4">
      <div className="row">
        <div className="col-md-8 mb-4 wow fadeIn">
          <Card id={MainText.id} title={MainText.title} text={MainText.text} />
          <AboutCard
            title={AboutText.title}
            image={AboutText.image}
            imageAlt={AboutText.imageAlt}
            name={AboutText.name}
            text={AboutText.text}
          />
        </div>
        <div className="col-md-4 mb-4 wow fadeIn">
          <CustomCard
            id={ContactText.id}
            text={ContactText.text}
            title={ContactText.title}
          />
          <CustomCard
            className="d-flex justify-content-center"
            id={GraphText.id}
            title={GraphText.title}
            text={GraphText.text}
          />
        </div>
      </div>
    </section>
  );
}

export default MainSection;
