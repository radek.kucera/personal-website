import React from "react";

function Card(props) {
  const text = props.text.map((line, index) => {
    return <p key={index}>{line}</p>;
  });
  return (
    <div className="card mb-4">
      <div className="card-body text-center">
        <p id={props.id} className="h5 my-4">
          {props.title}
        </p>
        {text}
      </div>
    </div>
  );
}

export default Card;
