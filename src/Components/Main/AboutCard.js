import React from "react";

function AboutCard(props) {
  return (
    <div className="card">
      <div className="card-header font-weight-bold">
        <span>{props.title}</span>
      </div>

      <div className="card-body">
        <div className="media d-block d-md-flex mt-3">
          <img
            className="d-flex mb-3 mx-auto z-depth-1"
            src={props.image}
            alt={props.imageAlt}
            style={{ width: "100px" }}
          />
          <div className="media-body text-center text-md-left ml-md-3 ml-0">
            <h5 className="mt-0 font-weight-bold">{props.name}</h5>
            {props.text}
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutCard;
