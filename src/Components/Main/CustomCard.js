import React from "react";

function CustomCard(props) {
  return (
    <div className="card mb-4">
      <div className="card-header" id={props.id}>
        {props.title}
      </div>
      <div className={props.className + " card-body"}>
        <div className="card-text">{props.text}</div>
      </div>
    </div>
  );
}

export default CustomCard;
