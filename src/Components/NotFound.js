import React from "react";

function NotFound() {
  return (
    <div className="container" style={{ paddingTop: "128px" }}>
      <h1 style={{ fontWeight: "bold" }}>Sorry, this page isnt't available.</h1>
      <p>The link you followed may be broken, or the page have been removed.</p>
    </div>
  );
}

export default NotFound;
