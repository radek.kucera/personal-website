import React from "react";
import Footer from "./Footer";
import Header from "./Header";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavItems from "../Constants/NavItems";
import NotFound from "./NotFound";

function App() {
  const routes = NavItems.map(route => {
    return (
      <Route
        key={route.title}
        path={route.path}
        exact={route.exact}
        component={route.component}
      />
    );
  });

  return (
    <Router>
      <Header />
      <Switch>
        {routes}
        <Route component={NotFound} />
      </Switch>
      <Footer />
    </Router>
  );
}

export default App;
