import React from "react";
import BrandLogo from "./BrandLogo";
import HamburgerMenu from "./HamburgerMenu";
import NavItems from "../../Constants/NavItems";
import { Link } from "react-router-dom";

function Navbar() {
  const items = NavItems.map(item => {
    return (
      <li key={item.title} className="nav-item">
        <Link to={item.path}>
          <span id={item.title} className="nav-link waves-effect">
            {item.title}
          </span>
        </Link>
      </li>
    );
  });

  return (
    <nav className="navbar fixed-top navbar-expand-lg navbar-light white scrolling-navbar">
      <div className="container">
        <BrandLogo title="Radek Kučera" />
        <HamburgerMenu />
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">{items}</ul>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
