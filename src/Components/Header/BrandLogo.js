import React from "react";

function BrandLogo(props) {
  return (
    <span className="navbar-brand waves-effect">
      <strong className="blue-text">{props.title}</strong>
    </span>
  );
}

export default BrandLogo;
