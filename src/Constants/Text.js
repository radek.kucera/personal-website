import React from "react";
import PieChart from "react-minimal-pie-chart";

export const MainText = {
  id: "main-text",
  title: "What is my page about?",
  text: [
    "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quae, ut rerum deserunt corporis ducimus at, deleniti ea alias dolor reprehenderit sit vel. Incidunt id illum doloribus, consequuntur maiores sed eligendi."
  ]
};

export const AboutText = {
  id: "about-text",
  title: "About author",
  name: "Radek Kučera",
  image: "./img/me.jpg",
  imageAlt: "Me",
  text:
    "I’m a frontend developer, currently studying Software Engineering at a University. Although I am still studying, I consider myself very flexible, fast and hardworking, and always open to learn new things. I am most interested in coding and programming languages, but also skilled in design."
};

export const ContactText = {
  id: "contact-text",
  title: "Contact me",
  text: <ContactInfo />
};

const Graph = [
  { title: "Javascript", value: 55, color: "#FFC107" },
  { title: "PHP", value: 10, color: "#673AB7" },
  { title: "Python", value: 15, color: "#03A9F4" },
  { title: "C#", value: 20, color: "#9C27B0" }
];

export const GraphText = {
  id: "graph-text",
  title: "Coding overview",
  text: <GraphElement />
};

function GraphElement() {
  return (
    <div id="chart" style={{ width: "256px", height: "256px" }}>
      <PieChart
        data={Graph}
        label={({ data, dataIndex }) =>
          (data[dataIndex] = Graph[dataIndex].title)
        }
        labelStyle={{
          fontSize: "6px",
          fill: "#121212",
          fontWeight: "bold"
        }}
      />
    </div>
  );
}

function ContactInfo() {
  return (
    <>
      <span>Radek Kučera</span>
      <br />
      <span>+420732231746</span>
      <br />
      <span>radakuceru@gmail.com</span>
    </>
  );
}
