import Main from "../Components/Main";
import Portfolio from "../Components/Portfolio";
import Gallery from "../Components/Gallery";

const NavItems = [
  {
    title: "Home",
    exact: true,
    path: "/",
    component: Main
  },
  {
    title: "Portfolio",
    path: "/portfolio/",
    component: Portfolio
  },
  {
    title: "Gallery",
    path: "/gallery/",
    component: Gallery
  }
];

export default NavItems;
